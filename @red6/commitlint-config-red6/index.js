const { merge } = require('lodash');

module.exports = merge(require('@commitlint/config-conventional'), {
  rules: {
    'header-max-length': [2, 'always', 100],
    'subject-case': [0],
    'subject-full-stop': [1, 'never', '.']
  }
});
