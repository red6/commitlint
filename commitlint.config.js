module.exports = {
  extends: ['./commitlint.base.js'],
  rules: {
    'scope-enum': [2, 'always', ['build', 'docker', 'config']]
  }
};
