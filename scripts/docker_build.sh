#!/usr/bin/env bash
_tag=$1

if [ -z "${_tag}" ]; then
    _tag=latest
fi

docker build \
  --build-arg BUILD_DATE=`date -u +"%Y-%m-%dT%H:%M:%SZ"` \
  --build-arg VERSION=dev-docker \
  --build-arg VCS_REF=`git rev-parse --short HEAD` \
  -t "registry.gitlab.com/red6/commitlint:${_tag}" .
