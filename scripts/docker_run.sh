#!/bin/bash

_tag=$1

if [ -z "${_tag}" ]; then
    _tag=latest
fi

docker run -it \
 --rm \
 --name red6__commitlint \
 -u 1000 \
 red6/commitlint:$_tag
