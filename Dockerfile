# ---- Base Node ----
FROM node:10-alpine AS base

ENV NODE_PATH=${NODE_PATH}:/opt/npm.global
ENV HOME /opt/

WORKDIR $HOME

RUN apk add --no-cache git

COPY package.json package-lock.json ./
COPY ./@red6/ ./@red6/

# ---- Dependencies ----
FROM base AS dependencies

RUN echo \
  && npm prune \
  && npm install --production \
  && mv node_modules npm.global \
  && ln -s ${HOME}npm.global/.bin/commitlint /usr/local/bin/commitlint \
  && rm package.json package-lock.json .npmrc \
  && echo

# ---- Build ---- 
FROM dependencies

ARG VERSION
ARG BUILD_DATE
ARG VCS_REF

LABEL maintainer="Sebastian Müller <sebastian.mueller@red6-es.de>" \
  org.label-schema.build-date=$BUILD_DATE \
  org.label-schema.version=$VERSION \
  org.label-schema.vcs-url="https://gitlab.com/red6/commitlint.git" \
  org.label-schema.vcs-ref=$VCS_REF \
  org.label-schema.schema-version="1.0.0-rc1"

WORKDIR /app

COPY commitlint.base.js ./

RUN echo "module.exports={extends:['./commitlint.base.js']}" > commitlint.config.js

ENTRYPOINT [ "commitlint" ]
