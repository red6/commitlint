# @red6/commitlint

## Run

```bash
docker run -v $(pwd)/.git:/app/.git registrypro.hanse-merkur.de/red6/commitlint:<version> --from=<sha>
```

**Note**: If you want commitlint to read from STDIN, don't forget to set the -i, --interactive flag to keep STDIN open.

## Configuration

This image ships with the `@red6/commitlint-config-red6` configuration preset packages.

You can extend this package by using a custom [configuration](https://commitlint.js.org/#/reference-configuration) file.

### Example

`commitlint.config.js`

```js
module.exports = {
  extends: ['./commitlint.base.js'],
  rules: {
    'scope-enum': [2, 'always', ['build', 'docker', 'config']]
  }
};
```

After this, you can mount this file into the docker container:

```bash
 -v $(pwd)/commitlint.config.js:/app/commitlint.config.js
```
